/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"
#include "math.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    DBG("MIDI message received");
    if (message.getVelocity() > 0) {
        frequency = MidiMessage::getMidiNoteInHertz(message.getNoteNumber());
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float twoPi = 2 * M_PI;
    float phaseIncrement = (twoPi * frequency)/sampleRate;
    float sine;
    
    
    while(numSamples--)
    {
        
        phasePosition += phaseIncrement;
        if (phasePosition > twoPi) {
            phasePosition -= twoPi;
        }
        
        sine = sin(phasePosition);
        
        *outL = sine * gain;
        *outR = sine * gain;
        
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    frequency = 440.f;
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
    
}

void Audio::audioDeviceStopped()
{

}
void Audio::setGain(float inGain)
{
    gain = inGain;
}