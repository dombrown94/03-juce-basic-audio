//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Dominic Brown on 11/13/14.
//
//

#ifndef __JuceBasicAudio__SinOscillator__
#define __JuceBasicAudio__SinOscillator__

#include <iostream>

class SinOscillator
{
public:
    
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    void getSample();
    void setFrequency();
    void setAmplitude();
    
private:
    float frequency;
    float phasePosition;
    float sampleRate;
};

#endif /* defined(__JuceBasicAudio__SinOscillator__) */
